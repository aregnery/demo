### 01 Business Continuity
---
Policy instructions for this control 
- [Busines Continuity Policy]()

The `Team Name` shall respond to all disruptions in the uptime of company inforamtion system (or other inforamtion systems that store or process company information or our customers infomration) in accordance with our Business Continuty Playbooks (if available) and the Business Continuity Plan

### Requirements Mapping
---
|~HIPAA Requirement|Label|Status|Implementation|
|---|---|---|---|
|[§164.308(a)(7)(ii)(B)](https://gitlab.com/aregnery/demo/-/requirements_management/requirements?page=1&state=opened&search=Disaster+Recovery+Plan&sort=created_desc)|Disaster Recovery Plan|||
|[164.308(a)(7)(ii)(C)]()|Emergency Operation Plan|||
|[164.310(a)(2)(i)]()|Contingency Operations|||
|[164.312(b)]()|Standard: Audit Controls|||
